defmodule WeatherWeb.PageController do
  use WeatherWeb, :controller
  alias Weather.Weathers

  @spec index(conn :: Plug.Conn.t(), params :: map()) :: Plug.Conn.t()
  def index(conn, _params) do
    render(conn, "index.html")
  end

  defp get_str_week_day(date) do
    case date |> Date.day_of_week() do
      1 -> "Lunes"
      2 -> "Martes"
      3 -> "Miércoles"
      4 -> "Jueves"
      5 -> "Viernes"
      6 -> "Sábado"
      7 -> "Domingo"
      _ -> :error
    end
  end

  @spec get_data_cities(conn :: Plug.Conn.t(), params :: map) :: conn :: Plug.Conn.t()
  def get_data_cities(conn, %{"data" => "weather"}) do
    cities = Weathers.list_cities() |> Enum.map(&Map.get(&1, :name))

    data_cities =
      Enum.map(cities, fn city ->
        case Weather.OpenWeatherMap.get_weather(city) do
          {:error, map} -> %{error: map}

          body ->
            %{
              name: city,
              humity: body["main"]["humidity"],
              temp: body["main"]["temp"],
              wind: body["wind"]["speed"]
            }
        end
      end)

    json(conn, %{cities: data_cities})
  end

  def get_data_cities(conn, %{"data" => "forecast", "city" => city}) do
    # List of forecasts for the following days that meet the condition
    # of being in a certain hour
    case Weather.OpenWeatherMap.get_forecast(city) do
      {:error, map} = error ->
        conn
        |> put_status(map |> Map.keys() |> Enum.at(0))
        |> json(error)

      %{"list" => all_forecasts} ->
        specific_forecast =
          Enum.filter(all_forecasts, &Regex.match?(~r/(06|12|18):00:00$/, &1["dt_txt"]))
          |> Enum.map(fn hour_forecast ->
            {:ok, date_utc} = DateTime.from_unix(hour_forecast["dt"])
            # Convert utc time to native time and subtract 5 hours to adjust to Mexico's time zone
            naive_date = date_utc |> DateTime.to_naive() |> NaiveDateTime.add(-(5 * 3600))
            week_day = date_utc |> get_str_week_day()

            %{
              week_day: week_day,
              month_day: naive_date.day,
              hour: hour_forecast["dt_txt"] |> String.split(" ") |> Enum.at(1),
              humity: hour_forecast["main"]["humidity"],
              temp: hour_forecast["main"]["temp"],
              wind: hour_forecast["wind"]["speed"]
            }
          end)
        # Creates a list of the days of the week that are found in the filtered days
        week_days =
          Enum.map(specific_forecast, fn %{week_day: week_days} -> week_days end) |> Enum.uniq()

        specific_days =
          Enum.map(week_days, fn day ->
            days =
              Enum.filter(specific_forecast, &(&1.week_day == day))
              |> Enum.map(fn day ->
                {_, map} = Map.pop(day, :week_day)
                map
              end)

            %{day => days}
          end)

        json(conn, %{days: specific_days})
    end
  end
end
