defmodule Weather.OpenWeatherMap do
  use Tesla

  plug Tesla.Middleware.BaseUrl, "https://api.openweathermap.org/data/2.5"
  plug Tesla.Middleware.JSON

  @spec get_weather(city :: String.t()) :: response_body :: map() | {:error, map()}
  def get_weather(city) do
    token = System.get_env("TOKEN_WEATHER_API")
    city = URI.encode(city)

    get("/weather?q=#{city}&appid=#{token}&units=metric")
    |> handle_http_request()
  end

  @spec get_forecast(city :: String.t()) :: response_body :: map() | {:error, map()}
  def get_forecast(city) do
    token = System.get_env("TOKEN_WEATHER_API")
    city = URI.encode(city)

    get("/forecast?q=#{city}&appid=#{token}&units=metric")
    |> handle_http_request()
  end

  @spec handle_http_request({:ok, map()}) :: response_body :: map() | {:error, map()}
  defp handle_http_request({:ok, %{status: 200, body: body}}), do: body

  defp handle_http_request({:ok, %{status: return, body: %{"message" => message}}}),
    do: {:error, %{return => message}}
end
