# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Weather.Repo.insert!(%Weather.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias Weather.Weathers.City
alias Weather.Repo

Repo.insert!(%City{
  name: "Ciudad de México"
})

Repo.insert!(%City{
  name: "Buenos Aires"
})

Repo.insert!(%City{
  name: "Bogotá"
})

Repo.insert!(%City{
  name: "Madrid"
})

Repo.insert!(%City{
  name: "Lima"
})
